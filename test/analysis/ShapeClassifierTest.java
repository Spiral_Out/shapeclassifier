package analysis;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.Collection;

import junit.framework.AssertionFailedError;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

@RunWith(Parameterized.class)
public class ShapeClassifierTest {
	public String input;
	public String expectedResult;
	public ShapeClassifier testObj;
	
	@Before
	public void initialize(){
		testObj = new ShapeClassifier();
	}
	public ShapeClassifierTest(String input, String expectedResult){
		this.input = input;
		this.expectedResult = expectedResult;
	}
	
	@Parameterized.Parameters
	public static Collection testInput(){
		return Arrays.asList(new Object [] [] {
				{"Line,Small,Yes,1,1","No"},
				{"Ellipse,Small,No,101,101","No"},
				{"Scalene,Large,Yes,4,4","No"},
				{"Rectangle,Large,No,1,1","No"},
				{"Square,Small,Yes,4095,4095","No"},
				{"Line,Large,Yes,4095,100","No"},
				{"Circle,Small,No,3,1","No"},
				{"Scalene,Small,No,4095,4903","No"},
				{"Isosceles,Large,No,2,1","No"},
				{"Rectangle,Small,Yes,4,3","No"},
				{"Square,Large,No,5,4","No"},
				{"Line,Large,No,299,301","No"},
				{"Circle,Small,Yes,1,4","No"},
				{"Square,Large,Yes,1,5","No"},
				{"Line,Small,No,5,1,2","No"},
				{"Line,Large,Yes,3,4095,5","No"},
				{"Line,Small,Yes,5,3,1","No"},
				{"Line,Large,No,3,1,5","No"},
				{"Line,Small,Yes,6,1,4","No"},
				{"Circle,Small,No,40,40","No"},
				{"Circle,Small,Yes,4095,201","No"},
				{"Ellipse,Small,No,3,5","Yes"},
				{"Ellipse,Large,Yes,5,3","No"},
				{"Scalene,Large,No,1,1","No"},
				{"Isosceles,Small,No,20,7,1","No"},
				{"Isosceles,Small,Yes,41,1,51","No"},
				{"Equilateral,Large,No,41,1,51","No"},
				{"Equilateral,Small,Yes,100,200,299","No"},
				{"Equilateral,Large,Yes,11,13,15","No"},
				{"Line,Large,Yes,4095","No"},
				{"Ellipse,Large,No,3","No"},
				{"Rectangle,Small,No,4095,4095,4095,4095","No"},
				{"Isosceles,Small,Yes,41,43,41,43","No"},
				{"Line,Large,Yes,3,1,1,1","No"},
				{"Rectangle,Large,No,4095,4095,21,21","Yes"},
				{"Rectangle,Large,No,4095,21,4095,21","Yes"},
				{"Rectangle,Large,No,4095,21,21,4095","Yes"},
				{"Rectangle,Large,No,4095,23,21,4093","No"},
				{"Line,Large,Yes,4095,4095,4095,4095","No"},
				{"Line,Large,Yes,4094,4094,4035,4095","No"},
				{"Equilateral,Small,Yes,1,1,1","No"},
				{"Isosceles,Large,No,4095,4095,2","No"},
				{"Isosceles,Small,Yes,1,1,3","No"},
				{"Isosceles,Small,Yes,2,2,4095","No"},
				{"Isosceles,Large,No,7,3,5","No"},
				{"Isosceles,Small,No,101,4095,4095","No"},
				{"Isosceles,Small,Yes,4095,4000,4095","No"},
				{"Isosceles,Large,No,4095,4000,4095","No"},
				{"Isosceles,Small,No,1,3,1","No"},
				{"Square,Large,No,4095,4095,4095,4095","Yes"}
		});
	}
		
	
	
	@Test
	public void evaluateGuessShapeShouldBeNoIfNotShape() {
		ShapeClassifier test = new ShapeClassifier();
//		String [] testInputs = {"Circle,Small,Yes,2","Line,Small,Yes,2"};
//		String [] testOutputs = {"Yes","No"};
//		System.out.print(testInputs.length);
//		for(int i = 0; i< testInputs.length; i++){
//			System.out.print(i);
//			try{
//				assertEquals("tt:"+(i), testOutputs[i],test.evaluateGuess(testInputs[i]));
//			}catch(Exception e){
//				System.out.println("fail");
//				e.printStackTrace();
//			}
		System.out.println("Test Case: " + this.input);
		System.out.println("ExpectedOutput: " + this.expectedResult);
		System.out.println("ReceivedOutput: " + this.testObj.evaluateGuess(this.input));
		assertEquals("TT:", this.expectedResult, this.testObj.evaluateGuess(this.input));
//		assertEquals("tt:1", "Yes",test.evaluateGuess("Circle,Small,Yes,2"));
//		assertEquals("tt:2","Yes",test.evaluateGuess("Line,Small,Yes,2"));
	}

//	@Test
//	public void evaluateGuessCircleShouldBeNoIf() {
//		ShapeClassifier test = new ShapeClassifier();
//		assertEquals("Guess:Circle,NumberParams:1", "No",test.evaluateGuess("Circle,Small,Yes,2"));
//		assertEquals("Guess:Line,NumberParams:1","Yes",test.evaluateGuess("Line,Small,Yes,2"));
//	}
	
}
